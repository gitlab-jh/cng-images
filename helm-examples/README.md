## Deploy using Helm

The GitLab JH images rails images can be used in the GitLab Helm chart by overriding the image locations of the GitLab services.

This will result in the JH images being used for those components. Other components continue to pull from their default locations as specified by the GitLab Helm Chart.

See the [GitLab Helm Chart Deployment documentation](https://docs.gitlab.com/charts/installation/deployment.html) for the general install instructions for the GitLab Helm Chart.


You can provides the overrides to your install using the example in this repository, or by specifying the overrides yourself locally.

### Pulling the overrides from example file

The overrides are available in this repository in the [jh-images.yml](jh-images.yml) file, which can be applied to your Helm Chart upgrade/install command.

For example:

```shell
helm repo add gitlab https://charts.gitlab.io/
helm repo update
helm upgrade --install gitlab gitlab/gitlab \
  --timeout 600s \
  -f https://gitlab.com/gitlab-jh/cng-images/-/tree/v13.10.0-jh/helm-examples/jh-images.yml \
  --set global.hosts.domain=example.com \
  --set global.hosts.externalIP=10.10.10.10 \
  --set certmanager-issuer.email=me@example.com
```

### Setting up the overrides locally

The overridden values should be placed in a local file and look like the following:

```yaml
global:
  enterpriseImages:
    migrations:
      repository: registry.gitlab.com/gitlab-jh/cng-images/gitlab-task-runner-jh
    sidekiq:
      repository: registry.gitlab.com/gitlab-jh/cng-images/gitlab-sidekiq-jh
    task-runner:
      repository: registry.gitlab.com/gitlab-jh/cng-images/gitlab-task-runner-jh
    webservice:
      repository: registry.gitlab.com/gitlab-jh/cng-images/gitlab-webservice-jh
    workhorse:
      repository: registry.gitlab.com/gitlab-jh/cng-images/gitlab-workhorse-jh
    geo-logcursor:
      repository: registry.gitlab.com/gitlab-jh/cng-images/gitlab-geo-logcursor
  kubectl:
    image:
      repository: registry.gitlab.com/gitlab-jh/cng-images/kubectl
      tag: 1.14.10
  certificates:
    image:
      repository: registry.gitlab.com/gitlab-jh/cng-images/alpine-certificates

gitlab:
  gitaly:
    image:
      repository: registry.gitlab.com/gitlab-jh/cng-images/gitaly
  gitlab-exporter:
    image:
      repository: registry.gitlab.com/gitlab-jh/cng-images/gitlab-exporter
  gitlab-shell:
    image:
      repository: registry.gitlab.com/gitlab-jh/cng-images/gitlab-shell
  mailroom:
    image:
      repository: registry.gitlab.com/gitlab-jh/cng-images/gitlab-mailroom
  gitlab-pages:
    image:
      repository: registry.gitlab.com/gitlab-jh/cng-images/gitlab-pages

registry:
  image:
    repository: registry.gitlab.com/gitlab-jh/cng-images/gitlab-container-registry

shared-secrets:
  selfsign:
    image:
      repository: registry.gitlab.com/gitlab-jh/cng-images/cfssl-self-sign

```

Then passed into the Helm upgrade or install command using the `-f` flag with your other values.

For example:

```shell
helm repo add gitlab https://charts.gitlab.io/
helm repo update
helm upgrade --install gitlab gitlab/gitlab \
  --timeout 600s \
  -f <my jh images file> \
  -f <my other values files> \
  --set global.hosts.domain=example.com \
  --set global.hosts.externalIP=10.10.10.10 \
  --set certmanager-issuer.email=me@example.com
```
