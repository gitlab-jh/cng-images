#!/bin/sh

if [ $# -eq 0 ] ; then
  echo 'No file specified'
  exit 0
fi

set -e

component_file=$1

JH_REGISTRY=${JH_REGISTRY:-"gitlab-jh.tencentcloudcr.com"}
JH_REGISTRY_USER=${JH_REGISTRY_USER:-"registry"}
JH_REGISTRY_NAMESPACE=${JH_REGISTRY_NAMESPACE:-"cng-images"}

echo "${CI_JOB_TOKEN}" | docker login -u "gitlab-ci-token" --password-stdin "${CI_REGISTRY}"
echo "Pulling images from dev registry"
while IFS=: read -r component tag; do
  docker pull -q "${CI_REGISTRY_IMAGE}/${component}:${tag}"
  docker tag "${CI_REGISTRY_IMAGE}/${component}:${tag}" "${JH_REGISTRY}/${JH_REGISTRY_NAMESPACE}/${component}:${tag}"
done < "${component_file}"

echo "${JH_REGISTRY_PASSWORD}" | docker login -u "${JH_REGISTRY_USER}" --password-stdin "${JH_REGISTRY}"
echo "Pushing images to jh registry"

while IFS=: read -r component tag; do
  docker push "${JH_REGISTRY}/${JH_REGISTRY_NAMESPACE}/${component}:${tag}"
  echo "${JH_REGISTRY}/${JH_REGISTRY_NAMESPACE}/${component}:${tag}" >> artifacts/cng_images.txt
done < "${component_file}"
