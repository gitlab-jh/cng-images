include: /.gitlab-ci.yml

# Redeclare templates to make them usable here
.job-base: &job-base
  retry: 1

.if_jh_var: &if_jh_var
  if: '$jh == "true"'

.if_jh_gitlab_version: &if_jh_gitlab_version
  if: '$GITLAB_VERSION =~ /-jh$/'

.if_jh_tag: &if_jh_tag
  if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+(-rc\d+)?-jh$/'

.if_jh_pipeline: &if_jh_pipeline
  if: '$JH_PIPELINE'

variables:
  JH_PIPELINE: 'true'
  CUSTOM_PIPELINE: 'true'
  SYNC_IMAGE: 'true'

.only-jh: &only-jh
  rules:
    - <<: *if_jh_pipeline
    - <<: *if_jh_tag

gitlab-elasticsearch-indexer:
  <<: *job-base
  <<: *only-jh

gitlab-rails-jh:
  extends: gitlab-rails-ee
  <<: *only-jh
  variables:
    COMPILE_ASSETS: 'true'
    GITLAB_NAMESPACE: 'gitlab-jh'
    EE_PROJECT: 'gitlab'
    FETCH_DEV_ARTIFACTS_PAT: '${FETCH_JH_ARTIFACTS_PAT}'

gitlab-task-runner-jh:
  extends: .job-base
  <<: *only-jh
  stage: phase-six
  script:
    - import_assets artifacts/ubi/gitlab-python.tar.gz
    - ruby_version=$(get_version gitlab-ruby)
    - rails_version=$(get_version gitlab-rails)
    - rails_container=$(cat artifacts/container_versions/gitlab-rails-ee_tag.txt)
    - gitaly_container=$(cat artifacts/container_versions/gitaly_tag.txt)
    - python_version=$(cat artifacts/container_versions/gitlab-python_tag.txt)
    - export FORCE_IMAGE_BUILDS="${FORCE_IMAGE_BUILDS-${FORCE_RAILS_IMAGE_BUILDS-false}}"
    - export CONTAINER_VERSION=($(echo -n "$rails_container$TARGET_VERSION" | sha1sum))
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-rails-jh:$rails_container${IMAGE_TAG_EXT}"
    - fi
    - build_if_needed --build-arg "FROM_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-jh"
                      --build-arg "TAG=$rails_container${IMAGE_TAG_EXT}"
                      --build-arg "PYTHON_TAG=${python_version}${IMAGE_TAG_EXT}"
                      --build-arg "S3CMD_VERSION=$S3CMD_VERSION"
                      --build-arg "GITLAB_VERSION=${GITLAB_VERSION}"
                      --build-arg "RAILS_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-jh:$rails_container${IMAGE_TAG_EXT}"
                      --build-arg "GITALY_IMAGE=$CI_REGISTRY_IMAGE/gitaly:$gitaly_container${IMAGE_TAG_EXT}"
    - if [ -n "$GITLAB_RELEASE_TAG" ]; then
    -   push_tags $GITLAB_RELEASE_TAG${IMAGE_TAG_EXT}
    - else
    -   push_tags $GITLAB_REF_SLUG${IMAGE_TAG_EXT}
    - fi
  needs:
    - gitlab-rails-jh
    - gitlab-python
    - gitaly

gitlab-geo-logcursor:
  <<: *job-base
  <<: *only-jh
  stage: phase-six
  script:
    - ruby_version=$(get_version gitlab-ruby)
    - rails_version=$(get_version gitlab-rails)
    - rails_container=$(cat artifacts/container_versions/gitlab-rails-ee_tag.txt)
    - export FORCE_IMAGE_BUILDS="${FORCE_IMAGE_BUILDS-${FORCE_RAILS_IMAGE_BUILDS-false}}"
    - export CONTAINER_VERSION=($(echo -n "$rails_container$TARGET_VERSION" | sha1sum))
    - export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-rails-jh:$rails_container${IMAGE_TAG_EXT}"
    - build_if_needed --build-arg "FROM_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-jh"
                      --build-arg "TAG=$rails_container${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_VERSION=${GITLAB_VERSION}"
                      --build-arg "RAILS_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-jh:$rails_container${IMAGE_TAG_EXT}"
    - if [ -n "$GITLAB_RELEASE_TAG" ]; then
    -   push_tags $GITLAB_RELEASE_TAG${IMAGE_TAG_EXT}
    - else
    -   push_tags $GITLAB_REF_SLUG${IMAGE_TAG_EXT}
    - fi
  needs:
    - gitlab-rails-jh

gitlab-webservice-jh:
  extends: .job-base
  <<: *only-jh
  stage: phase-six
  script:
    - import_assets artifacts/ubi/gitlab-python.tar.gz
    - import_assets artifacts/ubi/gitlab-logger.tar.gz
    - ruby_version=$(get_version gitlab-ruby)
    - go_dir_version=$(get_version gitlab-go)
    - go_version=($(echo -n "$BASE_VERSION$go_dir_version$GO_VERSION" | sha1sum))
    - logger_target_version=$(cat artifacts/container_versions/gitlab-logger_tag.txt)
    - rails_version=$(get_version gitlab-rails)
    - rails_container=$(cat artifacts/container_versions/gitlab-rails-ee_tag.txt)
    - python_version=$(cat artifacts/container_versions/gitlab-python_tag.txt)
    - export FORCE_IMAGE_BUILDS="${FORCE_IMAGE_BUILDS-${FORCE_RAILS_IMAGE_BUILDS-false}}"
    - export GITLAB_LOGGER_VERSION=($(echo -n "$go_version$logger_target_version$GITLAB_LOGGER_VERSION$(date -u +%D)" | sha1sum))
    - export CONTAINER_VERSION=($(echo -n "$rails_container$TARGET_VERSION" | sha1sum))
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-rails-jh:$rails_container${IMAGE_TAG_EXT}"
    - fi
    - build_if_needed --build-arg "FROM_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-jh"
                      --build-arg "TAG=$rails_container${IMAGE_TAG_EXT}"
                      --build-arg "PYTHON_TAG=${python_version}${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_VERSION=${GITLAB_VERSION}"
                      --build-arg "RAILS_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-jh:$rails_container${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_LOGGER_IMAGE=${CI_REGISTRY_IMAGE}/gitlab-logger:${GITLAB_LOGGER_VERSION}${IMAGE_TAG_EXT}"
    - if [ -n "$GITLAB_RELEASE_TAG" ]; then
    -   push_tags $GITLAB_RELEASE_TAG${IMAGE_TAG_EXT}
    - else
    -   push_tags $GITLAB_REF_SLUG${IMAGE_TAG_EXT}
    - fi
  needs:
    - gitlab-rails-jh
    - gitlab-python
    - gitlab-logger

gitlab-sidekiq-jh:
  extends: .job-base
  <<: *only-jh
  stage: phase-six
  script:
    - import_assets artifacts/ubi/gitlab-python.tar.gz
    - import_assets artifacts/ubi/gitlab-logger.tar.gz
    - ruby_version=$(get_version gitlab-ruby)
    - go_dir_version=$(get_version gitlab-go)
    - go_version=($(echo -n "$BASE_VERSION$go_dir_version$GO_VERSION" | sha1sum))
    - logger_target_version=$(cat artifacts/container_versions/gitlab-logger_tag.txt)
    - rails_version=$(get_version gitlab-rails)
    - rails_container=$(cat artifacts/container_versions/gitlab-rails-ee_tag.txt)
    - python_version=$(cat artifacts/container_versions/gitlab-python_tag.txt)
    - export FORCE_IMAGE_BUILDS="${FORCE_IMAGE_BUILDS-${FORCE_RAILS_IMAGE_BUILDS-false}}"
    - export GITLAB_LOGGER_VERSION=($(echo -n "$go_version$logger_target_version$GITLAB_LOGGER_VERSION$(date -u +%D)" | sha1sum))
    - export CONTAINER_VERSION=($(echo -n "$rails_container$TARGET_VERSION" | sha1sum))
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   export BASE_IMAGE="$CI_REGISTRY_IMAGE/gitlab-rails-jh:$rails_container${IMAGE_TAG_EXT}"
    - fi
    - build_if_needed --build-arg "FROM_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-jh"
                      --build-arg "TAG=$rails_container${IMAGE_TAG_EXT}"
                      --build-arg "PYTHON_TAG=${python_version}${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_VERSION=${GITLAB_VERSION}"
                      --build-arg "RAILS_IMAGE=$CI_REGISTRY_IMAGE/gitlab-rails-jh:$rails_container${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_LOGGER_IMAGE=${CI_REGISTRY_IMAGE}/gitlab-logger:${GITLAB_LOGGER_VERSION}${IMAGE_TAG_EXT}"
    - if [ -n "$GITLAB_RELEASE_TAG" ]; then
    -   push_tags $GITLAB_RELEASE_TAG${IMAGE_TAG_EXT}
    - else
    -   push_tags $GITLAB_REF_SLUG${IMAGE_TAG_EXT}
    - fi
  needs:
    - gitlab-rails-jh
    - gitlab-python
    - gitlab-logger

gitlab-workhorse-jh:
  extends: .job-base
  <<: *only-jh
  stage: phase-six
  script:
    - rails_version=$(get_version gitlab-rails)
    - ruby_version=$(get_version gitlab-ruby)
    - go_dir_version=$(get_version gitlab-go)
    - go_version=($(echo -n "$BASE_VERSION$go_dir_version$GO_VERSION" | sha1sum))
    - git_version=$(cat artifacts/container_versions/git-base_tag.txt)
    - rails_container=$(cat artifacts/container_versions/gitlab-rails-ee_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$rails_container$go_version$TARGET_VERSION" | sha1sum))
    - if [ ! "$UBI_PIPELINE" = "true" ]; then
    -   export BASE_IMAGE="$CI_REGISTRY_IMAGE/git-base:$git_version${IMAGE_TAG_EXT}"
    - fi
    - build_if_needed --build-arg "GIT_TAG=${git_version}${IMAGE_TAG_EXT}"
                      --build-arg "GO_TAG=${go_version}${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_VERSION=$GITLAB_VERSION"
                      --build-arg "RAILS_VERSION=$rails_container${IMAGE_TAG_EXT}"
                      --build-arg "RUBY_VERSION=$ruby_version${IMAGE_TAG_EXT}"
                      --build-arg "GITLAB_EDITION=gitlab-rails-jh"
                      --build-arg "RUBY_IMAGE=${CI_REGISTRY_IMAGE}/gitlab-ruby:$ruby_version${IMAGE_TAG_EXT}"
    - if [ -n "$GITLAB_RELEASE_TAG" ]; then
    -   push_tags $GITLAB_RELEASE_TAG${IMAGE_TAG_EXT}
    - else
    -   push_tags $GITLAB_REF_SLUG${IMAGE_TAG_EXT}
    - fi
  needs:
    - git-base
    - gitlab-rails-jh
    - gitlab-ruby

list-images:
  image: "docker:git"
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
  services:
    - docker:${DOCKER_VERSION}-dind
  stage: release
  allow_failure: false
  script:
    - cat artifacts/images/* > image_versions.txt
    - cp image_versions.txt ci_registry_images.txt
    - sed -ie "s|^|$CI_REGISTRY_IMAGE/|" ci_registry_images.txt
    - cat ci_registry_images.txt
    - rm -rf artifacts/*
    - mv image_versions.txt artifacts/.
    - mv ci_registry_images.txt artifacts/.
  artifacts:
    paths:
      - artifacts/
  rules:
    - if: '$SYNC_IMAGE == "true"'
      when: never
    - <<: *if_jh_pipeline
    - <<: *if_jh_tag

sync-jh-images:
  image: "${DEPENDENCY_PROXY}docker:git"
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://docker:2375
  services:
    - docker:${DOCKER_VERSION}-dind
  stage: release
  allow_failure: false
  script:
    - cat artifacts/images/* > image_versions.txt
    - rm -rf artifacts/*
    - sh build-scripts/jh_docker_image_sync.sh image_versions.txt
    - mv image_versions.txt artifacts/.
    - cat artifacts/cng_images.txt
  artifacts:
    paths:
      - artifacts/
  rules:
    - if: '$SYNC_IMAGE != "true"'
      when: never
    - <<: *if_jh_pipeline
      when: manual
    - <<: *if_jh_tag
      when: manual